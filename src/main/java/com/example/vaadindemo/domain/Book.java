package com.example.vaadindemo.domain;

public class Book {
	
	public enum Category {
		
		CRIME,
		HORROR,
		FANTASY,
		DOCUMENTARY,
		COMEDY,
		ROMANCE
	}
	private String authorFirstName;
	private String authorLastName;
	private String title;
	private int yearOfPublishing;
	private Category category;
	
	public Book( String title, String authorLastName, String authorFirstName,
			int yearOfPublishing, Category category) {
		super();
		this.authorFirstName = authorFirstName;
		this.authorLastName = authorLastName;
		this.title = title;
		this.yearOfPublishing = yearOfPublishing;
		this.category = category;
	}

	public String getAuthorFirstName() {
		return authorFirstName;
	}

	public void setAuthorFirstName(String authorFirstName) {
		this.authorFirstName = authorFirstName;
	}

	public String getAuthorLastName() {
		return authorLastName;
	}

	public void setAuthorLastName(String authorLastName) {
		this.authorLastName = authorLastName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYearOfPublishing() {
		return yearOfPublishing;
	}

	public void setYearOfPublishing(int yearOfPublishing) {
		this.yearOfPublishing = yearOfPublishing;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
	
}
