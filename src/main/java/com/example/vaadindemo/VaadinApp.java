package com.example.vaadindemo;

import com.example.vaadindemo.domain.Book;
import com.example.vaadindemo.domain.Book.Category;
import com.vaadin.annotations.Title;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Title("Vaadin Demo App")
public class VaadinApp extends UI {

	private static final long serialVersionUID = 1L;

	
	private Book book = new Book("Witcher", "Sapkowski", "Andrzej", 1992, Category.FANTASY);
	private BeanItem<Book> bookItem = new BeanItem<Book>(book);
	
	
	
	@Override
	protected void init(VaadinRequest request) {
		
		final FormLayout form = new FormLayout();
		final FieldGroup binder = new FieldGroup(bookItem);
		
		ComboBox combobox = new ComboBox("Category");
		combobox.setInvalidAllowed(false);
		combobox.setNullSelectionAllowed(false);
		        
		combobox.addItems(Category.values());
		
		binder.bind(combobox, "category");
		
		
		form.addComponent(binder.buildAndBind("Author's last name", "authorLastName"));
		form.addComponent(binder.buildAndBind("Author's first name", "authorFirstName"));
		form.addComponent(binder.buildAndBind("Title", "title"));
		form.addComponent(binder.buildAndBind("Year of publishing", "yearOfPublishing"));
		form.addComponent(combobox);
				
		
		
		binder.setBuffered(true);

		binder.getField("title").setRequired(true);
		

		
		VerticalLayout vl = new VerticalLayout();
		vl.setMargin(true);
		vl.addComponent(form);
		
		
		setContent(vl);

	}

}
